//
//  UIViewController+Storyboard.swift
//  BoostTest
//
//  Created by Wan Rahafiz Bin Wan Abdul Rahim on 21/06/2021.
//

import UIKit

protocol VCPageViewModelInputs {
    
}

protocol VCPageViewModelOutputs {
    var pageId: Box<String?> { get }
    var pageName: Box<String?> { get }
}

protocol VCPageViewModelType {
    var inputs: VCPageViewModelInputs { get }
    var outputs: VCPageViewModelOutputs { get }
}

class VCPageViewModel: VCPageViewModelInputs, VCPageViewModelOutputs, VCPageViewModelType {
    enum GeneralPopUpHeight: CGFloat {
        case Biometric = 330
        case Shortcut = 200
        case DatePicker = 450
    }
    let pageId: Box<String?> = Box(nil)
    let pageName: Box<String?> = Box(nil)
    
    var inputs: VCPageViewModelInputs { return self }
    var outputs: VCPageViewModelOutputs { return self }
}

extension UITableViewCell {
    /*func setupAnimationView(loopFlag: Bool = false, animationContainerView: UIView, animatedView: inout LOTAnimationView?, animationName: String, contentMode: UIView.ContentMode = .scaleAspectFit) -> CGFloat? {
        //        guard let animationTitle = animationName else { return }
        guard let bundlePath = Bundle.main.path(forResource: animationName, ofType: "bundle") else { return nil }
        guard let bundle = Bundle(path: bundlePath) else { return nil }
        
        let _animatedView = LOTAnimationView.init(name: animationName, bundle: bundle)
        
        _animatedView.contentMode = contentMode
        _animatedView.frame = animationContainerView.bounds.bounds
        _animatedView.play()
        _animatedView.loopAnimation = loopFlag
        
        animationContainerView.addSubview(_animatedView)
        
        _animatedView.addBoundedConstraint(
            to: animationContainerView,
            insets: UIEdgeInsets.zero
        )
        
        animatedView = _animatedView
        return  _animatedView.animationDuration
    }*/
}

extension UIViewController {
    
    /*func setupAnimationView(loopFlag: Bool = false, animationContainerView: UIView, animatedView: inout LOTAnimationView?, animationName: String, contentMode: UIView.ContentMode = .scaleAspectFit) -> CGFloat? {
        //        guard let animationTitle = animationName else { return }
        guard let bundlePath = Bundle.main.path(forResource: animationName, ofType: "bundle") else { return nil }
        guard let bundle = Bundle(path: bundlePath) else { return nil }
        
        let _animatedView = LOTAnimationView.init(name: animationName, bundle: bundle)
        
        _animatedView.contentMode = contentMode
        _animatedView.frame = animationContainerView.bounds.bounds
        _animatedView.play()
        _animatedView.loopAnimation = loopFlag
        
        animationContainerView.addSubview(_animatedView)
        
        _animatedView.addBoundedConstraint(
            to: animationContainerView,
            insets: UIEdgeInsets.zero
        )
        
        animatedView = _animatedView
        return  _animatedView.animationDuration
    }*/

    func goToForgotPin() {
        let vc = UIViewController.vc("ResetTransactionPin",identifier: nil)
        self.navigationController?.pushViewController(vc,animated: true)
    }
    
    
    
    
        
    /*func showNoBiometricErrorAlert() {
        
        var header = "Pay with Biometric ID"
        var title = "Switch On"
        var content = "If you have biometric capabilities in your device, please switch them on in your phone settings.\nYou will then be able to make payment with this feature."
        
        
        let vcIdentifier = String(describing: GeneralActionPopupVC.self)
        guard let vc = UIViewController.vc(
            "GeneralPopup",
            identifier: vcIdentifier
            ) as? GeneralActionPopupVC else { return }
        
        vc.viewModel.headerLogo = UIImage(named: "icon_biometric_settings")
        vc.viewModel.header = header
        vc.viewModel.title = title
        vc.viewModel.content = content
        //        vc.delegate = self
        
        vc.viewModel.okHandler = {
            vc.dismiss(
                animated: true,
                completion: {
                    if let url = URL(string:UIApplication.openSettingsURLString) {
                        if UIApplication.shared.canOpenURL(url) {
                            if #available(iOS 10.0, *) {
                                UIApplication.shared.open(url, options: [:], completionHandler: nil)
                            } else {
                                UIApplication.shared.openURL(url)
                            }
                        }
                    }
            })
        }
        
        vc.viewModel.cancelHandler = {
            vc.dismiss(
                animated: true,
                completion: nil)
        }
        
        presentPopUpController(
            tapDismiss: false,
            controller: { () -> UIViewController in
                return vc
        })
    }*/
    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    static func vc<T>(_ storyboard: String, ofType type: T.Type) -> T? where T: UIViewController {
        return vc(storyboard, identifier: String(describing: type)) as? T
    }
    
    static func vc(_ storyboard: String, identifier: String?) -> UIViewController {
        let storyBoardInstance = UIStoryboard(name: storyboard, bundle: nil)
        
        if let _identifier = identifier {
            return storyBoardInstance.instantiateViewController(
                withIdentifier: _identifier
            )
        } else {
            return storyBoardInstance.instantiateInitialViewController()!
        }
    }
    
    func vc(_ storyboard: String? = nil, identifier: String? = nil) -> UIViewController {
        var _storyboard = self.storyboard!
        if storyboard != nil {
            _storyboard = UIStoryboard(name: storyboard!, bundle: nil)
        }
        
        if identifier != nil {
            return _storyboard.instantiateViewController(withIdentifier: identifier!)
        } else {
            return _storyboard.instantiateInitialViewController()!
        }
    }
    

    
    private func actionHandleBlock(action:(() -> Void)? = nil) {
        struct __ {
            static var actionList: [UIViewController : (() -> Void)] = [:]
        }
        
        if action != nil {
            __.actionList[self] =  action
        } else {
            //when button pressed , view will unload.
            if __.actionList[self] != nil {
                __.actionList[self]!()
                __.actionList.removeValue(forKey: self)
            }
        }
    }
    
    @objc private func pop() {
        self.actionHandleBlock()
        
        if self.navigationController?.viewControllers.count ?? 0 > 1 {
            self.navigationController?.popViewController(animated: true)
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }
    


    
    func handleBasicExceedLimitError() {
        navigationController?.pushViewController(UIStoryboard(name: "UpgradePremium", bundle: nil).instantiateInitialViewController()!, animated: true)
    }
    
    /*func handlePremiumExceedLimitError() {
        let idType = UserInfo.loadUser()?.idType as String? ?? ""
        let vc =  UIStoryboard(name: "TransferOutVerification", bundle: nil).instantiateViewController(withIdentifier: "TransferOutVerification") as! TransferOutVerificationController
        
        vc.viewModel.fromP2PFlow = true
        vc.setupController(isPassport: false, isDeleteAcc: false)
        
        if UserInfo.loadUser()?.idType != nil {
            if idType.uppercased() == "PASSPORT" {
                vc.setupController(isPassport: true, isDeleteAcc: false)
            }
        }
        
        navigationController?.pushViewController(vc, animated: true)
    }*/
    
    func bstGoBack(animated: Bool) {
        if navigationController != nil {
            navigationController?.popViewController(animated: animated)
        } else {
            dismiss(animated: true,
                    completion: nil)
        }
    }
    
    func navigateToScreen(identifier: String, storyBoardName: String, withPush: Bool = true) {
        let vc = UIViewController.vc(storyBoardName, identifier: identifier)
        if withPush {
            navigationController?.pushViewController(vc, animated: true)
        } else {
            present(vc, animated: true, completion: nil)
        }
    }
    
    /*@objc func unrecognizedTapAction() {
        let vcIdentifier = String(describing: GeneralActionPopupVC.self)
        guard let vc = UIViewController.vc(
            "GeneralPopup",
            identifier: vcIdentifier
            ) as? GeneralActionPopupVC else { return }
        
        let header = NSLocalizedString(
            "update_app_alert_header",
            comment: ""
        )
        let title = NSLocalizedString(
            "update_app_alert_title",
            comment: ""
        )
        let content = NSLocalizedString(
            "update_app_alert_content",
            comment: ""
        )
        
        
        vc.viewModel.headerLogo = UIImage(named: "sad")
        vc.viewModel.header = header
        vc.viewModel.title = title
        vc.viewModel.content = content
        //        vc.delegate = self
        
        vc.viewModel.okHandler = {
            vc.dismiss(
                animated: true,
                completion: {
                    AppDelegate.openURL(C.ForceUpdateURL)
            })
        }
        
        vc.viewModel.cancelHandler = {
            vc.dismiss(
                animated: true,
                completion: nil)
        }
        
        presentPopUpController(
            tapDismiss: false,
            controller: { () -> UIViewController in
                return vc
        })
    }*/
    
    /*func goToSendCredit(customer: Customer, qrCodePaymentId: String, isScanQr: Bool) {
        guard let vc = UIViewController.vc(
            "SendCreditAmount",
            identifier: nil
            ) as? SendCreditAmountController else { return }
        vc.customer = customer
        vc.isScanQr = isScanQr
        
        navigationController?.pushViewController(
            vc,
            animated: true
        )
    }*/
    
    /*func goToPaymentAmountVC(paymentDetails: PaymentQr, redirectDeeplink: URL? = nil, payViaDeeplinkFlow: Bool = false, promoCodeObject: BSTPromoCodeObject? = nil, canAddPromoCode: Bool = false, allowZeroAmount: Bool = false, nextAction: ((_ amount: Int) -> Void)? = nil) {
//        guard let vc = UIViewController.vc(
//            "PaymentAmount",
//            identifier: nil
//            ) as? PaymentAmountController else { return }
//
//        vc.paymentDetails = paymentDetails
//        vc.viewModel.redirectDeeplink = redirectDeeplink
//        vc.viewModel.payViaDeeplinkFlow = payViaDeeplinkFlow
//
//        // clevertap for outcome pay qr success
////        CleverTapExtension.init().scanAndPaySuccess(merchantName: paymentDetails.merchantName ?? "Merchant name", merchantCategory: paymentDetails.merchantCategory ?? "Merchant category", amount: "\(String(describing: paymentDetails.transactionAmount))", qrCode: paymentDetails.qrCodePaymentId ?? "Qr code id")
//
//        navigationController?.pushViewController(vc, animated: true)
        
        let identifier = String(describing: GeneralPaymentAmountVC.self)
        guard let vc = UIViewController.vc(
            "General",
            identifier: identifier
            ) as? GeneralPaymentAmountVC else { return }
        
        vc.viewModel.inputs.configureWith(details: paymentDetails, redirectDeeplink: redirectDeeplink, payViaDeeplinkFlow: payViaDeeplinkFlow, promoCodeObject: promoCodeObject, canAddPromoCode: canAddPromoCode, allowZeroAmount: allowZeroAmount)
        vc.viewModel.outputs.nextAction.value = nextAction
        
        navigationController?.pushViewController(vc, animated: true)
    }*/

    
    // To support passing bstGoBack method as selector
    @objc func bstGoBack(_ sender: AnyObject?) {
        bstGoBack(animated: true)
    }
    
    func bstGoBack() {
        bstGoBack(animated: true)
    }
    
    func popOutActivityView(_ message: String) {
        let shareItems: Array = [ message] as [Any]
        
        let activityViewController = UIActivityViewController(
            activityItems: shareItems,
            applicationActivities: nil
        )
        activityViewController.excludedActivityTypes = [
            UIActivity.ActivityType.print, UIActivity.ActivityType.postToWeibo,
            UIActivity.ActivityType.postToVimeo, UIActivity.ActivityType.saveToCameraRoll
        ]
        activityViewController.popoverPresentationController?.sourceView = self.view
        
        present(activityViewController, animated: true, completion: nil)
    }
    
    func UIColorFromRGB(rgbValue: UInt) -> UIColor {
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    /*func handleToRedirectBoostMissionScreen(toMissionHome: Bool, missionId: String? = nil) {
        SVProgressHUD.show()
        
        _ = Api.getTutorialInfo(msisdn: UserInfo.loadUser()?.Msisdn as String? ?? "", {[weak self] (result) in
            SVProgressHUD.dismiss()
            
            guard let strongSelf = self else { return }
            
            if let value = result.value {
                DispatchQueue.main.async {
                    strongSelf.handleGetBoostMissionInfo(response: value, toMissionHome: toMissionHome, missionId: missionId)
                }
                
            } else if let error = result.error {
                strongSelf.handleError(
                    error: error,
                    inlineBlock: nil,
                    overlayBlock: nil,
                    noneBlock: nil
                )
            }
        })
    }*/
    
    /*fileprivate func handleGetBoostMissionInfo(response: [BSTTutorialResponse], toMissionHome: Bool, missionId: String? = nil) {
        if response.count > 0 {
            let identifier = String(describing: BSTTutorialPVC.self)
            guard let vc = UIViewController.vc(
                "BSTTutorial",
                identifier: identifier
                ) as? BSTTutorialPVC else { return }
            
            var tutorialObjects = [BSTTutorialObject]()
            for tutorial in response {
                var tutorialObj = BSTTutorialObject()
                
                if let imageString = tutorial.imageUrl {
                    if let imageUrl = C.verifyUrl(imageString) {
                        tutorialObj.imageUrl = imageUrl
                    } else {
                        tutorialObj.animationName = imageString
                    }
                }
                
                tutorialObj.title = tutorial.title
                tutorialObj.desc = tutorial.subtitle
                tutorialObj.buttonTitle = tutorial.buttonTitle
                tutorialObjects.append(tutorialObj)
            }
            
            vc.viewModel.outputs.tutorialObjects.value = tutorialObjects
            vc.viewModel.outputs.hideStatusBar.value = false
            navigationController?.pushViewController(vc, animated: true)
        } else {
            if let missionId = missionId {
                getAllMissionsBriefApi(missionId: missionId)
            } else {
                if toMissionHome {
                    let identifier = String(describing: BoostMissionViewController.self)
                    guard let vc = UIViewController.vc(
                        "BoostMission",
                        identifier: identifier
                        ) as? BoostMissionViewController else { return }
                    
                    // clevertap for act home mission
                    CleverTapExtension.init().actHomeMission(source: "Deeplink")
                    
                    navigationController?.pushViewController(vc, animated: true)
                } else {
                    let identifier = String(describing: PickMissionViewController.self)
                    guard let vc = UIViewController.vc(
                        "BoostMission",
                        identifier: identifier
                        ) as? PickMissionViewController else { return }
                    navigationController?.pushViewController(vc, animated: true)
                }
            }
        }
    }*/
    
    /*func getAllMissionsBriefApi(missionId: String) {
        guard let msisdn = UserInfo.loadUser()?.Msisdn as String? else { return }
        
        SVProgressHUD.show()
        
        _ = Api.getAllMissionsBrief(msisdn: msisdn.digits, missionId: missionId, {[weak self] (result) in
            SVProgressHUD.dismiss()
            
            guard let strongSelf = self else { return }
            
            if let missions = result.value?.missions, let _ = missions.filter({$0.missionId == missionId}).first {
                DispatchQueue.main.async {
                    let identifier = String(describing: MissionDetailsViewController.self)
                    guard let vc = UIViewController.vc(
                        "BoostMission",
                        identifier: identifier
                        ) as? MissionDetailsViewController else { return }
                    vc.viewModel.outputs.selectedMissionId.value = missionId
                    vc.viewModel.outputs.missionPageType.value = .brief
                    strongSelf.navigationController?.pushViewController(vc, animated: true)
                    return
                }
            } else {
                DispatchQueue.main.async {
                    let identifier = String(describing: PickMissionViewController.self)
                    guard let vc = UIViewController.vc(
                        "BoostMission",
                        identifier: identifier
                        ) as? PickMissionViewController else { return }
                    strongSelf.navigationController?.pushViewController(vc, animated: true)
                }
            }
        })
    }*/
}
