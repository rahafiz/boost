//
//  ContactDetailsVC.swift
//  BoostTest
//
//  Created by Wan Rahafiz Bin Wan Abdul Rahim on 20/06/2021.
//

import UIKit

class ContactDetailsVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var saveBtn: UIButton!
    @IBOutlet weak var cancelBtn: UIButton!
    
    let viewModel: ContactDetailsVCViewModelType = ContactDetailsVCViewModel()
    var dataSource = ContactDetailsVCDS()
    var cellHeightsDict = NSMutableDictionary()
    var isAdd: Bool = true
    var firstName: String = ""
    var lastName: String = ""
    var email: String?
    var phone: String?
    var contactIndex: Int = 0

    
    @IBAction func saveBtnPressed(_ sender: Any) {
        
        if isAdd {
            if validate() == true{
                
                var params = DemoData()
                params.firstName = self.firstName
                params.lastName = self.lastName
                
                if let email = self.email {
                    //contactList[0].email = email
                    params.email = email
                }
                if let phone = self.phone {
                    //contactList[0].phone = phone
                    params.phone = phone

                }
                
                let tempArray: [DemoData] = [params]
                contactList.append(contentsOf: tempArray)

                fresult = true
                
                if fresult == true{
                    writeToFile(location: subUrl!)
                    fresult = false
                }

                self.dismiss(animated: true, completion: {
                    
                    NotificationCenter.default.post(name: Notification.Name("save_data"), object: nil)
                    
                })
                
            }else {
                let alert = UIAlertController(title: "Reminder:", message: "First Name & Last Name are required", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        } else {
            if validate() == true{
                
                contactList[contactIndex].firstName = self.firstName
                contactList[contactIndex].lastName = self.lastName
                
                if self.email == "" {
                    contactList[contactIndex].email = self.email

                }
                if self.phone == "" {
                    contactList[contactIndex].phone = self.phone

                }
                
                fresult = true
                
                if fresult == true{
                    writeToFile(location: subUrl!)
                    fresult = false
                }
                
                self.dismiss(animated: true, completion: {
                    
                    NotificationCenter.default.post(name: Notification.Name("save_data"), object: nil)
                    
                })

            }else {
                let alert = UIAlertController(title: "Reminder:", message: "First Name & Last Name are required", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }
        
        
    }
    
    @IBAction func cancelBtnPressed(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        setupView()
        setupNavBar()
        setupListener()
        hideKeyboardWhenTappedAround()

    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        self.navigationController?.navigationBar.isHidden = true

    }

    
    private func setupView() {
        
        tableView.estimatedRowHeight = UITableView.automaticDimension
        tableView.rowHeight = UITableView.automaticDimension
        tableView.separatorStyle = .none
        
        if isAdd {
            saveBtn.isUserInteractionEnabled = false
            saveBtn.alpha = 0.2
        } else {
            saveBtn.isUserInteractionEnabled = true
            saveBtn.alpha = 1
        }

    }
    
    
    private func setupNavBar() {
        /*self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.view.backgroundColor = UIColor.white
        self.navigationController?.navigationBar.layoutIfNeeded()
        self.addBackButtonInNavItem(barStyle: .default)*/
        
    }

    private func setupListener() {
        tableView.dataSource = dataSource
        tableView.delegate = self
        
        dataSource.registerClasses(tableView: tableView)
        
        viewModel.outputs.headerImageVM.bind{ [weak self] value in
            DispatchQueue.main.async {
                guard let strongSelf = self else {return}
                strongSelf.dataSource.set(headerImage: value)
                strongSelf.tableView.reloadData()
                strongSelf.tableView.layoutIfNeeded()
            }
        }
        
        viewModel.outputs.textextInputVM.bind{ [weak self] value in
            DispatchQueue.main.async {
                guard let strongSelf = self else {return}
                strongSelf.dataSource.set(inputs: value)
                strongSelf.tableView.reloadData()
                strongSelf.tableView.layoutIfNeeded()
            }
        }
        

    }
    
    @objc private func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardSize.height, right: 0)
        }
    }

    @objc private func keyboardWillHide(notification: NSNotification) {
        tableView.contentInset = .zero
    }
    
    func validate() -> Bool{
        if (self.firstName.isEmpty || self.lastName.isEmpty) {
            return false
        }
        return true
    }
    
    func writeToFile(location: URL) {
        do{
            let encoder = JSONEncoder()
            encoder.outputFormatting = .prettyPrinted
            let JsonData = try encoder.encode(contactList)
            try JsonData.write(to: location)
        }catch{}
    }
        
}

//MARK: Tableview Cell Delegate
extension ContactDetailsVC: UITableViewDelegate {
      func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        /*if let height = cellHeightsDict[indexPath] {
            return height
        }*/
        if let height = cellHeightsDict.object(forKey: indexPath) as? CGFloat {
            return height
        }
        
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.selectionStyle = .none
        //cellHeightsDict[indexPath] = cell.frame.height
        cellHeightsDict.setObject(cell.frame.height, forKey: indexPath as NSCopying)

        if let cell = cell as? ContactTextFieldTVCTableViewCell{
            cell.delegate = self
        }
    }
    
}

extension ContactDetailsVC: UITextFieldDelegate {
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        
        return true
    }
}

extension ContactDetailsVC: ContactTextFieldTVCDelegate {
    func doneButtonClickedAction(class: ContactTextFieldTVCTableViewCell, isAdd: Bool) {
        if isAdd {
            saveBtn.isUserInteractionEnabled = isAdd
            saveBtn.alpha = 1
            
               
        }
    }
    
    func editButtonClickedAction(class: ContactTextFieldTVCTableViewCell, contactDetails: ContactTextFieldTVCViewModelType) {
        
        if let firstName = contactDetails.outputs.textFirstName.value {
            self.firstName = firstName
        }
        if let lastName = contactDetails.outputs.textLastName.value {
            self.lastName = lastName
        }
        if let email = contactDetails.outputs.textEmail.value {
            self.email = email
        }
        if let phone = contactDetails.outputs.textPhone.value {
            self.phone = phone
        }
    }
    
    func showErrorPopup(class: ContactTextFieldTVCTableViewCell) {
        
    }
    
}



