//
//  HeaderImageTVC.swift
//  BoostTest
//
//  Created by Wan Rahafiz Bin Wan Abdul Rahim on 20/06/2021.
//

import UIKit
import Library

class HeaderImageTVC: UITableViewCell {

    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var headerImgView: UIImageView!
    
    let viewModel: HeaderImageTVCViewModelType = HeaderImageTVCViewModel()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setupView()
        setupListener()
    }

    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override var intrinsicContentSize: CGSize {
        let size = containerView.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize)
        
        return size
    }
    
    fileprivate func setupView() {
        
        headerImgView.layer.cornerRadius = headerImgView.bounds.width / 2
        
        //configureCardCVCell()
    }
    
    fileprivate func setupListener() {
        
        viewModel.outputs.imgBgColor.bind { [weak self] value in
            guard let strongSelf = self else { return }
            
            strongSelf.headerImgView.tintColor = value
            
        }
        
        viewModel.outputs.contactImage.bind { [weak self] value in
            guard let strongSelf = self else { return }
            guard let value = value else { return }
            
            strongSelf.headerImgView.image = UIImage(named: value)
            
        }

    }
    
}

// MARK: View Layout
extension HeaderImageTVC: ValueCell {
    func configureWith(value: HeaderImageTVCViewModelType) {
        viewModel.outputs.imgBgColor.value = value.outputs.imgBgColor.value
        viewModel.outputs.contactImage.value = value.outputs.contactImage.value

    }
      
}

