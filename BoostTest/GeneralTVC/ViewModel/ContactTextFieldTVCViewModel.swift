//
//  ContactTextFieldTVCViewModel.swift
//  BoostTest
//
//  Created by Wan Rahafiz Bin Wan Abdul Rahim on 21/06/2021.
//

import Foundation
protocol ContactTextFieldTVCViewModelInputs {
    

  
}

protocol ContactTextFieldTVCViewModelOutputs {

    var textFirstName: Box<String?> { get }
    var textLastName: Box<String?> { get }
    var textEmail: Box<String?> { get }
    var textPhone: Box<String?> { get }
    var isAdd: Box<Bool> { get }




}

protocol ContactTextFieldTVCViewModelType {
    var inputs: ContactTextFieldTVCViewModelInputs { get }
    var outputs: ContactTextFieldTVCViewModelOutputs { get }
}

class ContactTextFieldTVCViewModel: ContactTextFieldTVCViewModelInputs, ContactTextFieldTVCViewModelOutputs, ContactTextFieldTVCViewModelType {
    
    
    
    let textFirstName: Box<String?> = Box(nil)
    let textLastName: Box<String?> = Box(nil)
    let textEmail: Box<String?> = Box(nil)
    let textPhone: Box<String?> = Box(nil)
    let isAdd: Box<Bool> = Box(true)


    
    
    var inputs: ContactTextFieldTVCViewModelInputs { return self }
    var outputs: ContactTextFieldTVCViewModelOutputs { return self }
    
    init() {
        // Data layout injection
        
    }
    
}
