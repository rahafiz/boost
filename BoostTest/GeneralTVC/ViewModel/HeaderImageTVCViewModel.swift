//
//  HeaderImageTVCViewModel.swift
//  BoostTest
//
//  Created by Wan Rahafiz Bin Wan Abdul Rahim on 20/06/2021.
//

import Foundation
import UIKit

protocol HeaderImageTVCViewModelInputs {
    

  
}

protocol HeaderImageTVCViewModelOutputs {

    var imgBgColor: Box<UIColor> { get }
    var contactImage: Box<String?> { get }


}

protocol HeaderImageTVCViewModelType {
    var inputs: HeaderImageTVCViewModelInputs { get }
    var outputs: HeaderImageTVCViewModelOutputs { get }
}

class HeaderImageTVCViewModel: HeaderImageTVCViewModelInputs, HeaderImageTVCViewModelOutputs, HeaderImageTVCViewModelType {
    
    let imgBgColor: Box<UIColor> = Box(UIColor.white)
    let contactImage: Box<String?> = Box(nil)
    
    
    var inputs: HeaderImageTVCViewModelInputs { return self }
    var outputs: HeaderImageTVCViewModelOutputs { return self }
    
    init() {
        // Data layout injection
        
    }
    
}
