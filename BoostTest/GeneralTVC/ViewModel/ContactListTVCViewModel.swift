//
//  ContactListTVCViewModel.swift
//  BoostTest
//
//  Created by Wan Rahafiz Bin Wan Abdul Rahim on 20/06/2021.
//

import Foundation
protocol ContactListTVCViewModelInputs {
    

  
}

protocol ContactListTVCViewModelOutputs {

    var firstName: Box<String?> { get }
    var lastName: Box<String?> { get }
    var email: Box<String?> { get }
    var phone: Box<String?> { get }



}

protocol ContactListTVCViewModelType {
    var inputs: ContactListTVCViewModelInputs { get }
    var outputs: ContactListTVCViewModelOutputs { get }
}

class ContactListTVCViewModel: ContactListTVCViewModelInputs, ContactListTVCViewModelOutputs, ContactListTVCViewModelType {
    
    
    let firstName: Box<String?> = Box(nil)
    let lastName: Box<String?> = Box(nil)
    let email: Box<String?> = Box(nil)
    let phone: Box<String?> = Box(nil)
    
    
    var inputs: ContactListTVCViewModelInputs { return self }
    var outputs: ContactListTVCViewModelOutputs { return self }
    
    init() {
        // Data layout injection
        
    }
    
}
