//
//  ContactListTVC.swift
//  BoostTest
//
//  Created by Wan Rahafiz Bin Wan Abdul Rahim on 20/06/2021.
//

import UIKit
import Library
protocol ContactListTVCDelegate: class {

}


class ContactListTVC: UITableViewCell {

    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var contactImgView: UIImageView!
    @IBOutlet weak var contactLbl: UILabel!
    
    let viewModel: ContactListTVCViewModelType = ContactListTVCViewModel()
    weak var delegate: ContactListTVCDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setupView()
        setupListener()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override var intrinsicContentSize: CGSize {
        let size = containerView.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize)
        
        return size
    }
    
    fileprivate func setupView() {
        
        contactImgView.layer.cornerRadius = contactImgView.bounds.width / 2
        
        //configureCardCVCell()
    }
    
    fileprivate func setupListener() {
        
        viewModel.outputs.firstName.bind { [weak self] value in
            guard let strongSelf = self else { return }
            guard let value = value else { return }
            
            strongSelf.contactLbl.text = value
            
        }
    }

    
}

// MARK: View Layout
extension ContactListTVC: ValueCell {
    func configureWith(value: ContactListTVCViewModelType) {
        viewModel.outputs.firstName.value = value.outputs.firstName.value
        viewModel.outputs.lastName.value = value.outputs.lastName.value
        viewModel.outputs.email.value = value.outputs.email.value
        viewModel.outputs.phone.value = value.outputs.phone.value

    }
      
}

