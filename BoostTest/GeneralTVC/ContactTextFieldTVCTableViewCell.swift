//
//  ContactTextFieldTVCTableViewCell.swift
//  BoostTest
//
//  Created by Wan Rahafiz Bin Wan Abdul Rahim on 21/06/2021.
//

import UIKit
import Library

protocol ContactTextFieldTVCDelegate: class {
    
    

    func doneButtonClickedAction(class: ContactTextFieldTVCTableViewCell, isAdd: Bool)
    func editButtonClickedAction(class: ContactTextFieldTVCTableViewCell, contactDetails: ContactTextFieldTVCViewModelType)

    
    func showErrorPopup(class: ContactTextFieldTVCTableViewCell)

}

class ContactTextFieldTVCTableViewCell: UITableViewCell {

    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var phoneTextField: UITextField!
    
    let viewModel: ContactTextFieldTVCViewModelType = ContactTextFieldTVCViewModel()
    weak var delegate: ContactTextFieldTVCDelegate?


    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setupView()
        setupListener()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override var intrinsicContentSize: CGSize {
        let size = containerView.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize)
        
        return size
    }
    
    fileprivate func setupView() {
        
        //firstNameTextField.inputView = UIView()
        //lastNameTextField.inputView = UIView()

        //configureCardCVCell()
        firstNameTextField.delegate = self
        lastNameTextField.delegate = self
        emailTextField.delegate = self
        phoneTextField.delegate = self
    }
    
    fileprivate func setupListener() {
        
        viewModel.outputs.textFirstName.bind { [weak self] value in
            guard let strongSelf = self else { return }
            guard let value = value else { return }
            
            strongSelf.firstNameTextField.text = value
            
        }
        
        viewModel.outputs.textLastName.bind { [weak self] value in
            guard let strongSelf = self else { return }
            guard let value = value else { return }
            
            strongSelf.lastNameTextField.text = value
            
        }

        viewModel.outputs.textEmail.bind { [weak self] value in
            guard let strongSelf = self else { return }
            guard let value = value else { return }
            
            strongSelf.emailTextField.text = value
            
        }

        viewModel.outputs.textPhone.bind { [weak self] value in
            guard let strongSelf = self else { return }
            guard let value = value else { return }
            
            strongSelf.phoneTextField.text = value
            
        }



    }
}

// MARK: View Layout
extension ContactTextFieldTVCTableViewCell: ValueCell {
    func configureWith(value: ContactTextFieldTVCViewModelType) {
        viewModel.outputs.isAdd.value = value.outputs.isAdd.value
        viewModel.outputs.textFirstName.value = value.outputs.textFirstName.value
        viewModel.outputs.textLastName.value = value.outputs.textLastName.value
        viewModel.outputs.textEmail.value = value.outputs.textEmail.value
        viewModel.outputs.textPhone.value = value.outputs.textPhone.value


    }
      
}

extension ContactTextFieldTVCTableViewCell: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
 
        //let current = self.getSelectedTextField()
    
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if textField.tag == 0 {
            viewModel.outputs.textFirstName.value = textField.text
            delegate?.editButtonClickedAction(class: self, contactDetails: viewModel)
        } else if textField.tag == 1 {
            viewModel.outputs.textLastName.value = textField.text
            delegate?.editButtonClickedAction(class: self, contactDetails: viewModel)
        } else if textField.tag == 2 {
            viewModel.outputs.textEmail.value = textField.text
            delegate?.editButtonClickedAction(class: self, contactDetails: viewModel)
        } else if textField.tag == 3 {
            viewModel.outputs.textPhone.value = textField.text
            delegate?.editButtonClickedAction(class: self, contactDetails: viewModel)
        }

        textField.resignFirstResponder()

    }

    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {

        return true
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {

        let nextTag = textField.tag + 1

        if let nextResponder = textField.superview?.viewWithTag(nextTag) {
        
            nextResponder.becomeFirstResponder()
        } else {
            delegate?.doneButtonClickedAction(class: self, isAdd: viewModel.outputs.isAdd.value)
            textField.resignFirstResponder()
        }

        return true


    }
}


