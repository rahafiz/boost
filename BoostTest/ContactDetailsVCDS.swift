//
//  ContactDetailsVCDS.swift
//  BoostTest
//
//  Created by Wan Rahafiz Bin Wan Abdul Rahim on 20/06/2021.
//

import Foundation
import Library
import UIKit

class ContactDetailsVCDS: ValueCellDataSource {
    enum Section: Int {
        case HeaderImage
        case Input
            
    }
    
    
    func set(headerImage: HeaderImageTVCViewModelType?) {
        let section = Section.HeaderImage.rawValue
             
        self.clearValues(section: section)
        
        if let headerImage = headerImage {
            self.set(
                values: [headerImage],
                cellClass: HeaderImageTVC.self,
                inSection: section
            )
        }
    }
    
    func set(inputs: ContactTextFieldTVCViewModelType?) {
        let section = Section.Input.rawValue
             
        self.clearValues(section: section)
        
        if let inputs = inputs {
            self.set(
                values: [inputs],
                cellClass: ContactTextFieldTVCTableViewCell.self,
                inSection: section
            )
        }
    }

    
        
    override func registerClasses(tableView: UITableView?) {
        tableView?.registerCellNibForClass(HeaderImageTVC.self)
        tableView?.registerCellNibForClass(ContactTextFieldTVCTableViewCell.self)


    }
    
    
    
    override func configureCell(tableCell cell: UITableViewCell, withValue value: Any) {
        switch (cell, value) {
        case let (cell as HeaderImageTVC, value as HeaderImageTVCViewModelType):
            cell.configureWith(value: value)
        case let (cell as ContactTextFieldTVCTableViewCell, value as ContactTextFieldTVCViewModelType):
            cell.configureWith(value: value)
        default:
            assertionFailure("")
        }
    }
    
}
