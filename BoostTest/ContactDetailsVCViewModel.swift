//
//  ContactDetailsVCViewModel.swift
//  BoostTest
//
//  Created by Wan Rahafiz Bin Wan Abdul Rahim on 20/06/2021.
//

import Foundation
import UIKit

protocol ContactDetailsVCViewModelInputs {
    

  
}

protocol ContactDetailsVCViewModelOutputs {

    var headerImageVM: Box<HeaderImageTVCViewModelType?> { get }
    var textextInputVM: Box<ContactTextFieldTVCViewModelType?> { get }



}

protocol ContactDetailsVCViewModelType {
    var inputs: ContactDetailsVCViewModelInputs { get }
    var outputs: ContactDetailsVCViewModelOutputs { get }
}

class ContactDetailsVCViewModel: ContactDetailsVCViewModelInputs, ContactDetailsVCViewModelOutputs, ContactDetailsVCViewModelType {
    
    
    let headerImageVM: Box<HeaderImageTVCViewModelType?>
    let textextInputVM: Box<ContactTextFieldTVCViewModelType?>

    var inputs: ContactDetailsVCViewModelInputs { return self }
    var outputs: ContactDetailsVCViewModelOutputs { return self }
    
    init() {
        // Data layout injection
        
        let tempHeaderVM = HeaderImageTVCViewModel()
        tempHeaderVM.outputs.contactImage.value = "empty_contact_list"
        tempHeaderVM.outputs.imgBgColor.value = UIColor(named: "#ff8c00") ?? UIColor.white
        
        headerImageVM = Box(tempHeaderVM)
        
        let tempTextInputVM = ContactTextFieldTVCViewModel()
        
        textextInputVM = Box(tempTextInputVM)
        
    }
    
}
