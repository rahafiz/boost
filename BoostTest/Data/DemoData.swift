//
//  DemoData.swift
//  BoostTest
//
//  Created by Wan Rahafiz Bin Wan Abdul Rahim on 21/06/2021.
//

import Foundation

struct DemoData: Codable {
    var id: String
    var firstName: String
    var lastName: String
    var email: String?
    var phone: String?
    
    init(id: String = "",
             firstName: String = "",
             lastName: String = "", email: String = "", phone: String = "") {
            
        self.id = id
        self.firstName = firstName
        self.lastName = lastName
        self.email = email
        self.phone = phone
    }

}
