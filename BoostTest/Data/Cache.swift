//
//  Cache.swift
//  BoostTest
//
//  Created by Wan Rahafiz Bin Wan Abdul Rahim on 21/06/2021.
//

import Foundation

struct Cache {

    static func clear() {
        UserDefaults.standard.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
    }
    
    static func setContactId(value: String, keyName: String) {
        UserDefaults.standard.set(value, forKey: keyName)
    }
    
    static func getContactId(keyName: String) -> String? {
        return UserDefaults.standard.string(forKey: keyName)
    }

    
    static func setFirstName(value: String, keyName: String) {
        UserDefaults.standard.set(value, forKey: keyName)
    }
    
    static func getFirstName(keyName: String) -> String? {
        return UserDefaults.standard.string(forKey: keyName)
    }
    
    static func setLastName(value: String, keyName: String) {
        UserDefaults.standard.set(value, forKey: keyName)
    }
    
    static func getLastName(keyName: String) -> String? {
        return UserDefaults.standard.string(forKey: keyName)
    }
    
    static func setEmail(value: String, keyName: String) {
        UserDefaults.standard.set(value, forKey: keyName)
    }
    
    static func getEmail(keyName: String) -> String? {
        return UserDefaults.standard.string(forKey: keyName)
    }
    
    static func setPhoneNumber(value: String, keyName: String) {
        UserDefaults.standard.set(value, forKey: keyName)
    }
    
    static func getPhoneNumber(keyName: String) -> String? {
        return UserDefaults.standard.string(forKey: keyName)
    }
    
    static func setContactArray(value: [[String: String]], keyName: String) {
        UserDefaults.standard.set(value, forKey: keyName)
    }
    

}
