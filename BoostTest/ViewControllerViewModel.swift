//
//  ViewControllerViewModel.swift
//  BoostTest
//
//  Created by Wan Rahafiz Bin Wan Abdul Rahim on 20/06/2021.
//

import Foundation
import UIKit

protocol ViewControllerViewModelInputs {
    

  
}

protocol ViewControllerViewModelOutputs {

    var contactListVM: Box<[ContactListTVCViewModelType]?> { get }
    var emptyContactListIcon: Box<UIImage?> { get }
    var emptyContactListTitle: Box<String> { get }
    var emptyContactListDesc: Box<String> { get }
    var isEmptyList: Box<Bool> { get }
    

}

protocol ViewControllerViewModelType {
    var inputs: ViewControllerViewModelInputs { get }
    var outputs: ViewControllerViewModelOutputs { get }
}

class ViewControllerViewModel: ViewControllerViewModelInputs, ViewControllerViewModelOutputs, ViewControllerViewModelType {
    
    var contactListVM: Box<[ContactListTVCViewModelType]?> = Box(nil)
    let emptyContactListIcon: Box<UIImage?> = Box(UIImage(named:"empty_contact_state"))
    let emptyContactListTitle: Box<String> = Box("You don't have any contact list yet.")
    let emptyContactListDesc: Box<String> = Box("Let's create one")
    let isEmptyList: Box<Bool> = Box(true)

    
    var inputs: ViewControllerViewModelInputs { return self }
    var outputs: ViewControllerViewModelOutputs { return self }
    
    init() {
        // Data layout injection
        
    }
    
}
