//
//  ViewController.swift
//  BoostTest
//
//  Created by Wan Rahafiz Bin Wan Abdul Rahim on 20/06/2021.
//

import UIKit
import DZNEmptyDataSet

var contactList: [DemoData] = []
var fm = FileManager.default
var fresult: Bool = false
var subUrl: URL?
var mainUrl: URL? = Bundle.main.url(forResource: "data", withExtension: "json")



class ViewController: UIViewController {

    
    @IBOutlet weak var addBtn: UIButton!
    @IBOutlet weak var tableView: UITableView!
    
    let viewModel: ViewControllerViewModelType = ViewControllerViewModel()
    var dataSource = ViewControllerDS()
    var cellHeightsDict = NSMutableDictionary()
    
    
    @IBAction func addBtnPressed(_ sender: Any) {
        
        let identifier = String(describing: ContactDetailsVC.self.self)
        guard let vc = UIViewController.vc(
            "Main",
            identifier: identifier
            ) as? ContactDetailsVC else { return }
        
        /*vc.viewModel.outputs.headerImageVM.value?.outputs.contactImage.value = "empty_contact_state"
        vc.viewModel.outputs.headerImageVM.value?.outputs.imgBgColor.value = UIColor(named: "#ff8c00") ?? UIColor.white
        vc.viewModel.outputs.mainTitleVM.value?.outputs.title.value = "Main Information"
        vc.viewModel.outputs.firstNameVM.value?.outputs.txtFieldTitle.value = "First Name"
        vc.viewModel.outputs.lastNameVM.value?.outputs.txtFieldTitle.value = "Last Name"
        vc.viewModel.outputs.secondTitleVM.value?.outputs.title.value = "Sub Information"
        vc.viewModel.outputs.emailVM.value?.outputs.txtFieldTitle.value = "Email"
        vc.viewModel.outputs.phoneNumber.value?.outputs.txtFieldTitle.value = "Phone Number"*/

        vc.isAdd = true
        self.navigationController?.present(vc, animated: true, completion: nil)

    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        NotificationCenter.default.addObserver(self, selector: #selector(refreshData(notification:)), name: NSNotification.Name(rawValue: "save_data"), object: nil)
        setupView()
        setupNavBar()
        setupListener()
        getData()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        self.navigationController?.navigationBar.isHidden = true

    }

    
    private func setupView() {
        
        tableView.estimatedRowHeight = UITableView.automaticDimension
        tableView.rowHeight = UITableView.automaticDimension
        tableView.separatorStyle = .none

    }
    
    
    private func setupNavBar() {
        /*self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.view.backgroundColor = UIColor.white
        self.navigationController?.navigationBar.layoutIfNeeded()
        self.addBackButtonInNavItem(barStyle: .default)*/
        
    }

    private func setupListener() {
        tableView.dataSource = dataSource
        tableView.delegate = self
        tableView.emptyDataSetSource = self
        tableView.emptyDataSetDelegate = self

        
        dataSource.registerClasses(tableView: tableView)
        
        viewModel.outputs.contactListVM.bind{ [weak self] value in
            DispatchQueue.main.async {
                guard let strongSelf = self else {return}
                strongSelf.dataSource.set(list: value)
                strongSelf.tableView.reloadData()
                strongSelf.tableView.layoutIfNeeded()
            }
        }
    }
    
    private func readLocalFile(forName name: String) -> Data? {
        do {
            if let bundlePath = Bundle.main.path(forResource: name,
                                                 ofType: "json"),
                let jsonData = try String(contentsOfFile: bundlePath).data(using: .utf8) {
                return jsonData
            }
        } catch {
            print(error)
        }
        
        return nil
    }

    
    func getData() {
        do {
            let documentDirectory = try fm.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
            subUrl = documentDirectory.appendingPathComponent("data.json")
            loadFile(mainPath: mainUrl!, subPath: subUrl!)
        } catch {
            print(error)
        }
    }
    
    func loadFile(mainPath: URL, subPath: URL){
        if fm.fileExists(atPath: subPath.path){
            decodeData(pathName: subPath)
            
            if viewModel.outputs.contactListVM.value?.count == 0{
                decodeData(pathName: mainPath)
            }
            
        }else{
            decodeData(pathName: mainPath)
        }
        
        //self.tableView.reloadData()
    }

    func decodeData(pathName: URL){
        do{
            let jsonData = try Data(contentsOf: pathName)
            let decoder = JSONDecoder()
            contactList = try decoder.decode([DemoData].self, from: jsonData)
            
            if contactList.count > 0 {
                var tempListVMs = [ContactListTVCViewModel]()
                
                for perContact in contactList {
                    let tempListVM = ContactListTVCViewModel()
                    tempListVM.outputs.firstName.value = perContact.firstName
                    tempListVM.outputs.lastName.value = perContact.lastName
                    tempListVM.outputs.email.value = perContact.email
                    tempListVM.outputs.phone.value = perContact.phone

                    
                    tempListVMs.append(tempListVM)
                }
                
                viewModel.outputs.contactListVM.value = tempListVMs
                viewModel.outputs.isEmptyList.value = false
                
            }
        } catch {
            
        }
    }
    
    func goToContactDetails(firstName: String, lastName: String, email: String?, phone: String?, contactIndex: Int) {
        
        let identifier = String(describing: ContactDetailsVC.self)
        guard let vc = UIViewController.vc(
            "Main",
            identifier: identifier
            ) as? ContactDetailsVC else { return }
        
        let tempHeaderVM = HeaderImageTVCViewModel()
        tempHeaderVM.outputs.contactImage.value = "empty_contact_list"
        tempHeaderVM.outputs.imgBgColor.value = UIColor(named: "#ff8c00") ?? UIColor.white
        
        vc.viewModel.outputs.headerImageVM.value = tempHeaderVM
        vc.contactIndex = contactIndex
        
        let tempTextInputVM = ContactTextFieldTVCViewModel()
        tempTextInputVM.outputs.textFirstName.value = firstName
        tempTextInputVM.outputs.textLastName.value = lastName
        
        if let email = email {
            tempTextInputVM.outputs.textEmail.value = email

        }
        
        if let phone = phone {
            tempTextInputVM.outputs.textPhone.value = phone


        }
        
        vc.viewModel.outputs.textextInputVM.value = tempTextInputVM
        vc.isAdd = false
        
        navigationController?.present(vc, animated: true, completion: nil)

    }
    
    @objc private func refreshData(notification: NSNotification) {
        
        self.getData()
    }



}

//MARK: Tableview Cell Delegate
extension ViewController: UITableViewDelegate {
      func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        /*if let height = cellHeightsDict[indexPath] {
            return height
        }*/
        if let height = cellHeightsDict.object(forKey: indexPath) as? CGFloat {
            return height
        }
        
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.selectionStyle = .none
        //cellHeightsDict[indexPath] = cell.frame.height
        cellHeightsDict.setObject(cell.frame.height, forKey: indexPath as NSCopying)

        if let cell = cell as? ContactListTVC{
            cell.delegate = self
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        guard let contacts = viewModel.outputs.contactListVM.value else { return }
        if contacts.count > 0 {
            let selectedContact = contacts[indexPath.row]
            guard let firstName = selectedContact.outputs.firstName.value else { return }
            guard let lastName = selectedContact.outputs.lastName.value else { return }
            guard let email = selectedContact.outputs.email.value else { return }
            guard let phone = selectedContact.outputs.phone.value else { return }

            goToContactDetails(firstName: firstName, lastName: lastName, email: email, phone: phone, contactIndex: indexPath.row)
        }
    }
    
}

extension ViewController: DZNEmptyDataSetSource {
    func image(forEmptyDataSet scrollView: UIScrollView!) -> UIImage! {
        return viewModel.outputs.emptyContactListIcon.value
    }
    
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        let myAttribute = [ NSAttributedString.Key.foregroundColor: UIColor.black]
        let myAttrString = NSAttributedString(string: viewModel.outputs.emptyContactListTitle.value, attributes: myAttribute)
        return myAttrString
        
    }
    
    func description(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        let myAttribute = [ NSAttributedString.Key.foregroundColor: UIColor.black]
        let myAttrString = NSAttributedString(string: viewModel.outputs.emptyContactListDesc.value, attributes: myAttribute)
        return myAttrString
        
    }
}

extension ViewController: DZNEmptyDataSetDelegate {
    func emptyDataSetShouldBeForced(toDisplay scrollView: UIScrollView!) -> Bool {
        if viewModel.outputs.isEmptyList.value {

            return true
            
        } else {
            
            return false

        }
        
    }
    
    func emptyDataSetShouldAllowScroll(_ scrollView: UIScrollView!) -> Bool {
        return true
    }
}

extension ViewController: ContactListTVCDelegate {
    
}


