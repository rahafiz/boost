//
//  ViewControllerDS.swift
//  BoostTest
//
//  Created by Wan Rahafiz Bin Wan Abdul Rahim on 20/06/2021.
//

import Foundation
import Library
import UIKit

class ViewControllerDS: ValueCellDataSource {
    enum Section: Int {
        case List
    }
    
    
    func set(list: [ContactListTVCViewModelType]?) {
        let section = Section.List.rawValue
             
        self.clearValues(section: section)
        
        if let list = list {
            self.set(
                values: list,
                cellClass: ContactListTVC.self,
                inSection: section
            )
        }
    }
    

        
    override func registerClasses(tableView: UITableView?) {
        tableView?.registerCellNibForClass(ContactListTVC.self)

    }
    
    
    
    override func configureCell(tableCell cell: UITableViewCell, withValue value: Any) {
        switch (cell, value) {
        case let (cell as ContactListTVC, value as ContactListTVCViewModelType):
            cell.configureWith(value: value)
        default:
            assertionFailure("")
        }
    }
    
}
