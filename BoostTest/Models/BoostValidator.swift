//
//  BoostValidator.swift
//  BoostTest
//
//  Created by Wan Rahafiz Bin Wan Abdul Rahim on 21/06/2021.
//

import Foundation
import Validator

struct StandardValidationError: ValidationError {
    
    public let message: String
    
    public init(message m: String) {
        message = m
    }
}

extension StandardValidationError: LocalizedError {
    
    public var errorDescription: String? {
        return message
    }
}

class BoostValidator: NSObject {
    
    static let shared: BoostValidator = BoostValidator()

    static let requiredError = StandardValidationError(message: NSLocalizedString(
        "common_error_required",
        comment: ""
    ))
        
    let passwordRules: ValidationRuleSet<String> = {
        var rules = ValidationRuleSet<String>()
        
        let min = 8
        let max = 100
        let lengthError = StandardValidationError(message: NSLocalizedString(
            "common_error_pass_range",
            comment: ""
        ))
                
        let lengthRule = ValidationRuleLength(min: min,
                                              max: max,
                                              error: lengthError)
        rules.add(rule: lengthRule)
        
        /*let uppercaseRequiredError = StandardValidationError(
            message: NSLocalizedString(
                "common_error_pass_uppercase_required",
                comment: ""
            )
        )*/
        let digitRequiredError = StandardValidationError(
            message: NSLocalizedString(
                "common_error_pass_digit_required",
                comment: ""
            )
        )
        //let upperCaseRule = ValidationRulePattern(pattern: "^(?=.*[A-Z]).*$",
        //                                          error: uppercaseRequiredError)
        //        let alphaRule = ValidationRulePattern(pattern: "^(?=.*[a-z]).*$",
        //                                               error: patternError)
        //let numericRule = ValidationRulePattern(pattern: "^(?=.*[0-9]).*$",
        //                                        error: digitRequiredError)
        
        //rules.add(rule: upperCaseRule)
        //        rules.add(rule: alphaRule)
        //rules.add(rule: numericRule)
        
        return rules
    }()
    
    let emailRules: ValidationRuleSet<String> = {
        var rules = ValidationRuleSet<String>()
        
        let emailPattern = EmailValidationPattern.standard
        let emailError = StandardValidationError(message: NSLocalizedString(
            "common_error_email",
            comment: ""
        ))
        let emailRule = ValidationRulePattern(pattern: emailPattern,
                                              error: emailError)
        
        rules.add(rule: emailRule)
        
        return rules
    }()
    
    let nameRules: ValidationRuleSet<String> = {
        var rules = ValidationRuleSet<String>()
        
        // Special character checking
        let specialCharError = StandardValidationError(message: NSLocalizedString(
            "common_error_name",
            comment: ""
        ))
        let specialCharPattern = ValidationRulePattern(pattern: "([a-zA-Z0-9 '/@])+",
                                                       error: specialCharError)
        
        // Digit checking
        let numCharError = StandardValidationError(message: NSLocalizedString(
            "common_error_name_no_digit",
            comment: ""
        ))
        let numCharPattern = ValidationRulePattern(pattern: "([^0123456789])+",
                                                   error: numCharError)
        
        rules.add(rule: specialCharPattern)
        rules.add(rule: numCharPattern)
        
        return rules
    }()
    
    let nicknameRules: ValidationRuleSet<String> = {
        var rules = ValidationRuleSet<String>()
        
        // Special character checking
        let specialCharError = StandardValidationError(message: NSLocalizedString(
            "common_error_nickname",
            comment: ""
        ))
        let specialCharPattern = ValidationRulePattern(pattern: "([a-zA-Z0-9 '/@])+",
                                                       error: specialCharError)
        
        // Digit checking
        let numCharError = StandardValidationError(message: NSLocalizedString(
            "common_error_nickname_no_digit",
            comment: ""
        ))
        let numCharPattern = ValidationRulePattern(pattern: "([^0123456789])+",
                                                   error: numCharError)
        
        rules.add(rule: specialCharPattern)
        rules.add(rule: numCharPattern)
        
        return rules
    }()
    
    let mobileRules: ValidationRuleSet<String> = {
        var rules = ValidationRuleSet<String>()
        
        let numberMinText = NSLocalizedString(
            "common_error_mobile_range",
            comment: ""
        )
        let numberRangeError = StandardValidationError(message: numberMinText)
        // Add 1 to the min and max for the 6 prefix
        let numberRangeRule = ValidationRuleLength(min: 9,
                                                   max: 11,
                                                   error: numberRangeError)
        
        let digitOnlyErrorText = NSLocalizedString(
            "common_error_mobile_digit_only",
            comment: ""
        )
        let digitOnlyError = StandardValidationError(message: digitOnlyErrorText)
        let digitOnlyRule = ValidationRulePattern(pattern: "([0-9])+",
                                                  error: digitOnlyError)
        
        rules.add(rule: numberRangeRule)
        rules.add(rule: digitOnlyRule)
        
        return rules
    }()
    
    let tacRules: ValidationRuleSet<String> = {
        var rules = ValidationRuleSet<String>()
        
        let tacLength = 6
        
        let numberMinText = NSLocalizedString(
            "common_error_tac",
            comment: ""
        )
        let numberRangeError = StandardValidationError(message: numberMinText)
        // Add 1 to the min and max for the 6 prefix
        let numberRangeRule = ValidationRuleLength(min: tacLength,
                                                   max: tacLength,
                                                   error: numberRangeError)
        
        let digitOnlyErrorText = NSLocalizedString(
            "common_error_tac",
            comment: ""
        )
        let digitOnlyError = StandardValidationError(message: digitOnlyErrorText)
        let digitOnlyRule = ValidationRulePattern(pattern: "([0-9])+",
                                                  error: digitOnlyError)
        
        rules.add(rule: numberRangeRule)
        rules.add(rule: digitOnlyRule)
        
        return rules
    }()
    
    let digitRules: ValidationRuleSet<String> = {
        var rules = ValidationRuleSet<String>()
        
        let digitOnlyErrorText = NSLocalizedString(
            "common_error_digit_only",
            comment: ""
        )
        
        let digitOnlyError = StandardValidationError(message: digitOnlyErrorText)
        let digitOnlyRule = ValidationRulePattern(
            pattern: "([0-9])+",
            error: digitOnlyError
        )
        
        rules.add(rule: digitOnlyRule)
        
        return rules
    }()
    
    let vehicleRules: ValidationRuleSet<String> = {
        var rules = ValidationRuleSet<String>()
        
        let vehicleOnlyErrorText = NSLocalizedString(
            "common_error_vehicle",
            comment: ""
        )
        
        let vehicleOnlyError = StandardValidationError(message: vehicleOnlyErrorText)
        let vehicleOnlyRule = ValidationRulePattern(
            pattern: "([A-Z0-9])+",
            error: vehicleOnlyError
        )
        
        rules.add(rule: vehicleOnlyRule)
        return rules
    }()
    
//    let currencyRules: ValidationRuleSet<String> = {
//        var rules = ValidationRuleSet<String>()
//
//        let currencyOnlyErrorText = NSLocalizedString(
//            "common_error_currency",
//            comment: ""
//        )
//
//        let currencyOnlyError = ValidationError(message: currencyOnlyErrorText)
//        let currencyOnlyRule = ValidationRulePattern(
//            pattern: "^([1-9]\\d{0,2}(,\\d{3})*|([1-9]\\d*))(\\.\\d{2})?$",
//            error: currencyOnlyError
//        )
//
//        rules.add(rule: currencyOnlyRule)
//        return rules
//    }()
    
    let requiredRules: ValidationRuleSet<String> = {
        var rules = ValidationRuleSet<String>()
        
        let lengthRule = ValidationRuleLength(min: 1,
                                              error: requiredError)
        rules.add(rule: lengthRule)
        
        return rules
    }()
    
    static func regexRules(regex: String,
                           message: String) -> ValidationRuleSet<String> {
        var rules = ValidationRuleSet<String>()
        
        let regexError = StandardValidationError(message: message)
        let regexRule = ValidationRulePattern(pattern: regex,
                                              error: regexError)
        
        rules.add(rule: regexRule)
        
        return rules
    }
    
    static func rangeRules(min: Int = 0,
                           max: Int = 0,
                           error: ValidationError? = nil) -> ValidationRuleSet<String> {
        var rules = ValidationRuleSet<String>()
        
        let lengthError = error ?? StandardValidationError(message: "Value should be between \(min) and \(max) characters")
        let lengthRule = ValidationRuleLength(min: min,
                                              max: max,
                                              error: lengthError)
        
        rules.add(rule: lengthRule)
        
        return rules
    }
    
    static func minRules(min: Int = 0,
                         error: ValidationError? = nil) -> ValidationRuleSet<String> {
        var rules = ValidationRuleSet<String>()
        
        let lengthError = error ?? StandardValidationError(message: "Value should be at least \(min) characters")
        let lengthRule = ValidationRuleLength(min: min,
                                              error: lengthError)
        
        rules.add(rule: lengthRule)
        
        return rules
    }
    
    static func maxRules(max: Int = 0,
                         error: ValidationError? = nil) -> ValidationRuleSet<String> {
        var rules = ValidationRuleSet<String>()
        
        let maxError = error ?? StandardValidationError(message: "Value should be no more than \(max) characters")
        let maxRule = ValidationRuleLength(max: max,
                                           error: maxError)
        
        rules.add(rule: maxRule)
        
        return rules
    }
    
    static func minValueRules(min: Float = 1.0,
                              error: ValidationError? = nil) -> ValidationRuleSet<Float> {
        var rules = ValidationRuleSet<Float>()
        
        let minError = error ?? StandardValidationError(message: "Value should be at least \(min)")
        let lengthRule = ValidationRuleComparison(min: min,
                                                  max: 1.0,
                                                  error: minError)
        
        rules.add(rule: lengthRule)
        
        return rules
    }
}
