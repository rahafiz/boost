//
//  StaticTableViewCell.swift
//  Library
//
//  Created by Wan Rahafiz Bin Wan Abdul Rahim on 03/06/2021.
//  Copyright © 2021 kwsp. All rights reserved.
//

import Foundation
import UIKit

open class StaticTableViewCell: UITableViewCell, ValueCell {
  open func configureWith(value _: Void) {}
}
