//
//  SharedFunctions.swift
//  Library
//
//  Created by Wan Rahafiz Bin Wan Abdul Rahim on 03/06/2021.
//  Copyright © 2021 kwsp. All rights reserved.
//

import UserNotifications



internal func classNameWithoutModule(_ class: AnyClass) -> String {
  return `class`
    .description()
    .components(separatedBy: ".")
    .dropFirst()
    .joined(separator: ".")
}

typealias SanitizedPledgeParams = (pledgeTotal: String, rewardIds: [String], locationId: String?)


public func ksr_pledgeAmount(
  _ pledgeAmount: Double,
  subtractingShippingAmount shippingAmount: Double?
) -> Double {
  guard let shippingAmount = shippingAmount, shippingAmount > 0 else { return pledgeAmount }

  let pledgeAmount = Decimal(pledgeAmount) - Decimal(shippingAmount)

  return (pledgeAmount as NSDecimalNumber).doubleValue
}


/*
 A helper that assists in rounding a Double to a given number of decimal places
 */
public func rounded(_ value: Double, places: Int16) -> Decimal {
  let roundingBehavior = NSDecimalNumberHandler(
    roundingMode: .bankers,
    scale: places,
    raiseOnExactness: true,
    raiseOnOverflow: true,
    raiseOnUnderflow: true,
    raiseOnDivideByZero: true
  )

  return NSDecimalNumber(value: value).rounding(accordingToBehavior: roundingBehavior) as Decimal
}

/*
 A helper that assists in rounding a Float to a given number of decimal places
 */
public func rounded(_ value: Float, places: Int16) -> Decimal {
  let roundingBehavior = NSDecimalNumberHandler(
    roundingMode: .bankers,
    scale: places,
    raiseOnExactness: true,
    raiseOnOverflow: true,
    raiseOnUnderflow: true,
    raiseOnDivideByZero: true
  )

  return NSDecimalNumber(value: value).rounding(accordingToBehavior: roundingBehavior) as Decimal
}











